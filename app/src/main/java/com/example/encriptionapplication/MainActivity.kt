package com.example.encriptionapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import javax.crypto.Cipher

class MainActivity : AppCompatActivity() {
    lateinit var cipher: Cipher
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")

        replace.setOnClickListener {
            val outputText = output_text.text.toString()
            orign_text.setText(outputText)
            output_text.text = ""
        }

        encode.setOnClickListener {
            output_text.text = getEncodeString(orign_text.text.toString())
        }

        decode.setOnClickListener {
            output_text.text = getDecodeString(orign_text.text.toString())
        }
    }

    private fun getDecodeString(text: String): String{
        return Base64.getEncoder().encodeToString(text.toByteArray())
    }

    private fun getEncodeString(text: String): String{
        return String(Base64.getDecoder().decode(text))
    }
}